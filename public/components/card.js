'use strict'
// Initial Template Code
const card = document.createElement('template')
card.template = {}


// 888    888 88888888888 888b     d888 888      
// 888    888     888     8888b   d8888 888      
// 888    888     888     88888b.d88888 888      
// 8888888888     888     888Y88888P888 888      
// 888    888     888     888 Y888P 888 888      
// 888    888     888     888  Y8P  888 888      
// 888    888     888     888   "   888 888      
// 888    888     888     888       888 88888888                            


card.template.html = ({}) => `
	<div class="container">
		<slot class="question" name="question"></slot>
		<div class="answers-container"></div>
		<slot class="submit" name="submit"></slot>
	</div>
`


// .d8888b.   .d8888b.   .d8888b.  
// d88P  Y88b d88P  Y88b d88P  Y88b 
// 888    888 Y88b.      Y88b.      
// 888         "Y888b.    "Y888b.   
// 888            "Y88b.     "Y88b. 
// 888    888       "888       "888 
// Y88b  d88P Y88b  d88P Y88b  d88P 
//  "Y8888P"   "Y8888P"   "Y8888P"  


card.template.css = ({
	container = {
		background: { color: 'white' }
	}
}) => `
  <style>
  	${boilerplate}
  	.container {
  		${background(container.background)}
  		height: 85vh;
  		display: flex;
  		flex-direction: column;
  	}
  	.question {
  		height: max-content;
  		max-height: 30%;
  		flex: 1000 0 auto;
  	}
  	.answers-container {
  		display: flex;
  		flex-flow: row wrap;
  		justify-content: space-around;
  		align-items: center;
  		overflow-y: auto;
  		flex: 1 1 auto;
      padding-bottom: 5%;
  	}
  </style>
`



// 8888888888 888      8888888888 888b     d888 8888888888 888b    888 88888888888 
// 888        888      888        8888b   d8888 888        8888b   888     888     
// 888        888      888        88888b.d88888 888        88888b  888     888     
// 8888888    888      8888888    888Y88888P888 8888888    888Y88b 888     888     
// 888        888      888        888 Y888P 888 888        888 Y88b888     888     
// 888        888      888        888  Y8P  888 888        888  Y88888     888     
// 888        888      888        888   "   888 888        888   Y8888     888     
// 8888888888 88888888 8888888888 888       888 8888888888 888    Y888     888     


card.innerHTML = card.template.css({}) + card.template.html({})
customElements.define('card-',
  class extends HTMLElement {
    constructor() {
      super()
      
      this
        .attachShadow({mode: 'open'})
        .appendChild(card.content.cloneNode(true))


			// Selected Elements
			const container = $(this)('.container')
			const question = $(`<question-></question->`, { slot: "question", className: 'question' })
			const answersContainer = $(this)('.answers-container')
			const submit = $('<button-crud></button-crud>')
			submit.setAttribute('text', 'Next')
			submit.setAttribute('type', 'button')

			const mode = 'grid'

			// Shared Functions
			const generateQuestion = () => question.setAttribute("text", globalData.cards[globalData.current].question)
			const generateAnswers = () => answersContainer.innerHTML = globalData.cards[globalData.current].answers.map(a => // Add Answers
				`<checkbox-label>${a}</checkbox-label>`
			).join('')

      container.style.height = globalData.elements.appHeight.value.parseInt() + 70 + 'vh'

			{// GENERATE QUESTIONS & ANSWERS START /* 2 */
				generateQuestion()
				$(this)('.question').parentNode.replaceChild(question, $(this)('.question'))
				$(this)('.submit').parentNode.replaceChild(submit, $(this)('.submit'))
				generateAnswers()
			}// GENERATE QUESTIONS & ANSWERS END

      {// ADD ANSWER ELEMENTS TO GLOBAL DATA START
        globalData.elements.inputs = Object.values(answersContainer.children)
      }// ADD ANSWER ELEMENTS TO GLOBAL DATA END

			// Variables To Calculate Card Width
			const getComputedWidth = (x) => parseFloat(window.getComputedStyle(x).width.replace('px', '')) // Gets width of element
			const currentContainerWidth = () => getComputedWidth(container) // Current Width Container
			const sqrtTotalAnswers = Math.floor(Math.sqrt(answersContainer.children.length)) // Square Root Of Total Answers
			const answerLongWidth = () => {
			return Object
				.values(answersContainer.children)
				.map(a => /* 1 */{
					return Math.ceil(getComputedWidth(a))
				})
				.reduce((a,b) => a > b ? a : b)
			}  // Width Of Longest Answer

			const answerAverageWidth = /* 1 */ () => Math.ceil(
				Object
					.values(answersContainer.children)
					.reduce((a,b) =>
						typeof a === 'object' ?
						getComputedWidth(a) + getComputedWidth(b) :
						a + getComputedWidth(b)
					) / answersContainer.children.length
			) // Width Of Average Of All Widths

			const answerContainerResize = () => {
				container.style.minWidth = answerLongWidth() + 'px'
				container.style.maxWidth = 
					mode === 'flex' ? sqrtTotalAnswers * answerAverageWidth() + (answersContainer.offsetWidth - answersContainer.clientWidth) + 'px':
					mode === 'grid' ? sqrtTotalAnswers * answerLongWidth() + (answersContainer.offsetWidth - answersContainer.clientWidth) + 'px' :
					console.error('Incorrect Mode')
			}
								
			const flex = () => {
				// Adjust Card Width Based On Average Answer Width
				for (let i = sqrtTotalAnswers; i > 0; i--) {
					if (currentContainerWidth() < i * answerAverageWidth()) continue
					else {
						container.style.width = i * answerAverageWidth() + (answersContainer.offsetWidth - answersContainer.clientWidth) + 'px'
						break
					}
				}
			}

			const grid = () => {
				// Variables
				const answers = Object.values(answersContainer.children)				
				const longestWidth = answers
					.map(a => getComputedWidth(a.shadowRoot.querySelector('.container')))
					.reduce((a,b) => a > b ? a : b)

				// Changes All Answer Widths To Matching Widths
				answers.map(a => a.shadowRoot.querySelector('.container').style.width = longestWidth + 'px')

				// Adjust Card Width Based On Longest Answer Width
				for (let i = sqrtTotalAnswers; i > 0; i--) {
					if (currentContainerWidth() < i * answerLongWidth()) continue
					else {
						container.style.width = i * answerLongWidth + (answersContainer.offsetWidth - answersContainer.clientWidth) + 'px'
						break
					}
				}
			}

			const answerRealign = () => {
				mode === 'grid' ? grid() : 
				mode === 'flex' ? flex() :
				null
			}

			// Calculate Width Of Card To Maintain Square Look

			$('question-decks').addEventListener('click', () => {
				answerRealign()
				answerContainerResize()				
			})
			
			// Calculate Width Of Card On Resize To Maintain Square Look
			window.addEventListener('resize', () => {
				container.style.width = ''
				answerRealign()
				answerContainerResize()
			})

			{// SUBMITION START
				submit.addEventListener('click', () => {
					const currentCount = globalData.current
					const questionCount = globalData.cards.length
					if (currentCount === questionCount || submit.disabled) {return} // Stop Function Once All Answers Have Been Finished
					submit.disabled ? null : submit.disabled = true
					const currentAnswers = Object.values(answersContainer.children)
					const currentQuestion =  globalData.cards[globalData.current].question
         


					{// STORE USER INPUTS START
						globalData.inputs[currentCount] = { questions: currentQuestion, answers: [] }
						currentAnswers
							.map(a =>
								globalData.inputs[currentCount].answers.push(a.shadowRoot.querySelector('.input').checked)
							)	
					}// STORE USER INPUTS END


					{// STORE ANSWER STATS START
						globalData.outputs[currentCount] = { 
							question:  globalData.cards[globalData.current].question, 
							answers: [], 
							selected: globalData.inputs[currentCount].answers.filter(a => a === true).length,
							select: {
								right: 0,
								wrong: 0,
							},
							unselect: {
								right: 0,
								wrong: 0,
							}
						}

						// Search For Correct Answers
						for (let i = 0, len = globalData.answersData.length; i < len; i++) 
							if (Object.values(globalData.answersData[i]).includes(currentQuestion)) {
								globalData.outputs[currentCount].relaventAnswers = Object.values(globalData.answersData[i])
								break
							}

						
						for (let i = 0, len = currentAnswers.length; i < len; i++) {
							// Checks Global Data For Correct Answer

							const correctAnswer = globalData.outputs[currentCount].relaventAnswers
								.some(a => a === globalData.cards[currentCount].answers[i])


							// Checks If Input Is Checked
							const selected = currentAnswers[i].shadowRoot.querySelector('.input').checked

							// Store Correct Answer Stats
							if (correctAnswer) {
								globalData.outputs[currentCount].answers.push('✅')
								if (selected) {
									globalData.score.right++
									globalData.outputs[currentCount].select.right++
									currentAnswers[i].shadowRoot.querySelector('.right').style.display = 'block'
                  globalMethods.newInputColor(globalData.elements.inputs[i], 'checkbox', globalData.color.right1)
									globalMethods.newInputColor(globalData.elements.inputs[i], 'background', globalData.color.right2)
                  
								}
								else {
									currentAnswers[i].shadowRoot.querySelector('.miss').style.display = 'block'
                  globalMethods.newInputColor(globalData.elements.inputs[i], 'checkbox', globalData.color.miss1)
									globalMethods.newInputColor(globalData.elements.inputs[i], 'background', globalData.color.miss2)
                  
								}
								if (!selected) globalData.outputs[currentCount].unselect.right++
							}
							// Store Incorrect Answer Stats
							else {
								globalData.outputs[currentCount].answers.push('❌')								
								if (selected) {
									globalData.score.wrong++
									globalData.outputs[currentCount].select.wrong++
									currentAnswers[i].shadowRoot.querySelector('.wrong').style.display= 'block'
                  globalMethods.newInputColor(globalData.elements.inputs[i], 'checkbox', globalData.color.wrong1)
									globalMethods.newInputColor(globalData.elements.inputs[i], 'background', globalData.color.wrong2)
                  
								}
								if (!selected) globalData.outputs[currentCount].unselect.wrong++
							}
						}
						
					}// STORE ANSWER STATS END


          {// Score Overview
            const selectedCorrect = globalData.outputs[globalData.outputs.length - 1].select.right
            const selectedWrong = globalData.outputs[globalData.outputs.length - 1].select.wrong
            const totalCorrect = parseInt(globalData.limit.correctAnswers)
            const rightSound = new Audio('correct.wav')
            const wrongSound = new Audio(`incorrect.wav`)
            if (selectedCorrect - selectedWrong === totalCorrect) {
              globalData.score.overview.push('🟩')
              rightSound.play()
              globalMethods.newSubmitColor(globalData.color.right2)
            }
            else if (selectedCorrect - selectedWrong > totalCorrect / 2) {
              globalData.score.overview.push('🟨')
              wrongSound.play()
              globalMethods.newSubmitColor(globalData.color.miss2)
            }
            else {
              globalData.score.overview.push('🟥')
              wrongSound.play()
              globalMethods.newSubmitColor(globalData.color.wrong2)
            }
          }// Score Overview

				
					{// GENERATE NEW QUESTION & ANSWERS START
						globalData.current++
						{// DISPLAY SCORE
							if (currentCount === questionCount - 1) {
                setTimeout(() => {
								  const score = document.querySelector('score-').shadowRoot
								  const right = {
									  numerator: score.querySelector('.right .numerator'),
									  denominator: score.querySelector('.right .denominator'),
								  }
	  
								  const wrong = {
									  numerator: score.querySelector('.wrong .numerator'),
									  denominator: score.querySelector('.wrong .denominator'),
								  }
								  right.numerator.textContent = globalData.score.right
								  right.denominator.textContent = globalData.total.right
								  wrong.numerator.textContent = globalData.score.wrong
								  wrong.denominator.textContent = globalData.total.wrong
								  
								  this.style.display = 'none'
								  document.querySelector('score-').style.display = 'block'
    						}, globalData.delay)
							}
              
						}// DISPLAY SCORE
						
						if (currentCount === questionCount - 1) {return} // Prevents From Trying To Push To Non-Existent Array Element
						
						setTimeout(() => {// Gives time to show wrong/right answer feedback
							generateQuestion()
							generateAnswers()
							globalData.elements.inputs = Object.values(answersContainer.children)
							{// REALIGN ELEMENTS START
								container.style.width = ''
								answerContainerResize()
								answerRealign()
							}// REALIGN ELEMENTS END
							submit.disabled = false
						}, globalData.delay)
					}// GENERATE NEW QUESTION & ANSWERS	END

					
				})
			}// SUBMITION END
//      let vh = window.innerHeight * 0.01;
//      document.documentElement.style.setProperty('--vh', `${vh}px`) // Quick fix for mobile
//      setTimeout(() => document.documentElement.style.setProperty('--vh', `${vh}px`), 5000)
    }
  }
)



// 88888888888 8888888888 888b     d888 8888888b.  888             d8888 88888888888 8888888888 
//     888     888        8888b   d8888 888   Y88b 888            d88888     888     888        
//     888     888        88888b.d88888 888    888 888           d88P888     888     888        
//     888     8888888    888Y88888P888 888   d88P 888          d88P 888     888     8888888    
//     888     888        888 Y888P 888 8888888P"  888         d88P  888     888     888        
//     888     888        888  Y8P  888 888        888        d88P   888     888     888        
//     888     888        888   "   888 888        888       d8888888888     888     888        
//     888     8888888888 888       888 888        88888888 d88P     888     888     8888888888 

// /* 1 */ Using Math.ceil due to calculating lengths up to a certain decimal place
// /* 2 */ Using .innerHTML due to .append/.appendChild not working for custom elements

/*  HTML TEMPLATE
	<card-
		mode=" grid | list | flex"
	></card->
*/ 
