'use strict'
// Initial Template Code
const questionDecks = document.createElement('template')
questionDecks.template = {}


// 888    888 88888888888 888b     d888 888      
// 888    888     888     8888b   d8888 888      
// 888    888     888     88888b.d88888 888      
// 8888888888     888     888Y88888P888 888      
// 888    888     888     888 Y888P 888 888      
// 888    888     888     888  Y8P  888 888      
// 888    888     888     888   "   888 888      
// 888    888     888     888       888 88888888                            


questionDecks.template.html = ({}) => `
	<div class="listing">
		<div class="settings">
			<div class="question-count">
				<label>Question Count</label>
				<input type="number" min="1" max="1000" value="5" class="question-number">
				<input type="range" min="1" max="1000" value="5" class="question-slider">
			</div>
			<div class="answer-count">
				<label>Answer Count</label>
				<input type="number" min="1" max="30" value="9" class="answer-number">
				<input type="range" min="1" max="30" value="9" class="answer-slider">
			</div>
			<div class="correctAnswer-count">
				<label>Correct Answer Count</label>
				<input type="number" min="1" max="3" value="3" class="correct-answer-number">
				<input type="range" min="1" max="3" value="3" class="correct-answer-slider">
			</div>
      <div class="app-height">
				<label>App Height (To Fix Mobile View)</label>
				<input type="number" min="0" max="30" value="15" class="app-height-number">
				<input type="range" min="0" max="30" value="15" class="app-height-slider">
			</div>
		</div>
		<button class="numbers">Numbers</button>
		<button class="pronouns">Pronouns</button>
		<button class="family">Family</button>
		<button class="time">Time</button>		
		<button class="color">Color</button>		
		<button class="greetings-and-farewells">Greetings & Farewells</button>		
		<button class="jobs">Jobs</button>		
		<button class="furniture">Furniture</button>	
	</div>
`


// .d8888b.   .d8888b.   .d8888b.  
// d88P  Y88b d88P  Y88b d88P  Y88b 
// 888    888 Y88b.      Y88b.      
// 888         "Y888b.    "Y888b.   
// 888            "Y88b.     "Y88b. 
// 888    888       "888       "888 
// Y88b  d88P Y88b  d88P Y88b  d88P 
//  "Y8888P"   "Y8888P"   "Y8888P"  


questionDecks.template.css = ({
	
}) => `
  <style>
		.listing {
			${ flex({ wrap: 'wrap', justify: 'center' })}
		}
		.settings {
			position: absolute;
			top: 0;
			left: 0;
		}
		.question-count, .answer-count, .correctAnswer-count {
			${ flex({ align: 'center' }) }
		}
		[type=number] { width: 60px;}
		[type=range] {width: 60px;}
  </style>
`



// 8888888888 888      8888888888 888b     d888 8888888888 888b    888 88888888888 
// 888        888      888        8888b   d8888 888        8888b   888     888     
// 888        888      888        88888b.d88888 888        88888b  888     888     
// 8888888    888      8888888    888Y88888P888 8888888    888Y88b 888     888     
// 888        888      888        888 Y888P 888 888        888 Y88b888     888     
// 888        888      888        888  Y8P  888 888        888  Y88888     888     
// 888        888      888        888   "   888 888        888   Y8888     888     
// 8888888888 88888888 8888888888 888       888 8888888888 888    Y888     888     


questionDecks.innerHTML = questionDecks.template.css({}) + questionDecks.template.html({})
customElements.define('question-decks',
  class extends HTMLElement {
    constructor() {
      super()
      
      this
        .attachShadow({mode: 'open'})
        .appendChild(questionDecks.content.cloneNode(true))

	    const numbers = $(this)('.numbers')
	    const pronouns = $(this)('.pronouns')
	    const family = $(this)('.family')
	    const time = $(this)('.time')
	    const color = $(this)('.color')
	    const greetingsAndFarewells = $(this)('.greetings-and-farewells')
	    const jobs = $(this)('.jobs')
      const furniture = $(this)('.furniture')
	    const question = {
		    slider: $(this)('.question-slider'),
		    number: $(this)('.question-number')	
	    }

	    const answer = {
		    slider: $(this)('.answer-slider'),
		    number: $(this)('.answer-number')	
	    }

	    const correctAnswer = {
		    slider: $(this)('.correct-answer-slider'),
		    number: $(this)('.correct-answer-number')	
	    }

      const appHeight = {
		    slider: $(this)('.app-height-slider'),
		    number: $(this)('.app-height-number')	
	    }

			// MAKE INTO COMPONENT
			const generate = (data) => {
				deckGen(data)
				this.style.display = 'none'
				const card = $('<card-></card->')
				card.setAttribute('mode', 'grid')
				document.body.prepend(card)
				globalData.answersData = data
			}
			[numbers, pronouns, family, time, color, greetingsAndFarewells, jobs, furniture]
				.forEach(a => 
					a.addEventListener('click', () => {
						globalData.limit.questions = question.number.value
						globalData.limit.answers = answer.number.value
						globalData.limit.correctAnswers = correctAnswer.number.value
						return generate(
							answers[
								a.className.split('')
									.map((a,b,c) => {
										if (a === '-') {
											c[b + 1] = c[b + 1].toUpperCase()
											return a
										}
										else return a
									})
									.join('')
									.replace(/-/gi, '')
							]
						)
					})
				)


			;[question, answer, correctAnswer, appHeight].forEach(({ number, slider}, b) => {
				const counters = ['question', 'answer', 'correctAnswer']
				slider.addEventListener('input', () => number.value = slider.value)
				number.addEventListener('keypress', e => !!e.key.match(/[0-9]/) ? null : e.preventDefault())
				number.addEventListener('keydown', e => e.keyCode === 13 && number.value === '' ? number.value = number.min : null)
				number.addEventListener('input', () => {
					if (parseInt(number.value) > parseInt(number.max)) {
						number.value = number.max
						slider.value = number.max
					}
					else if (number.value === '') slider.value = slider.min
					else slider.value = number.value
				})
				;[slider, number]
					.forEach(a => a.addEventListener('blur', 
						() => number.value === '0' ? number.value = number.min : null
					))
			})

      // Keep App Height Settings
      if (!!document.cookie) document.querySelector('.body').style.height = decodeURIComponent(document.cookie).split('=')[1] + 'vh'
      appHeight.map((a,b) => {
        if (a[0] === 'number') globalData.elements.appHeight = a[1]
        window.addEventListener('load', () => {
          const cookieElementHeight = decodeURIComponent(document.cookie).split('=')[1]
          if (!!cookieElementHeight)
            a[1].value = cookieElementHeight.parseInt() - 70
        })        
        a[1].addEventListener('input', () => {
          const date = new Date()
          const height = 70 + parseInt(a[1].value)
          date.setTime(date.getTime() + (365*24*60*60*1000))
          document.querySelector('.body').style.height = `${height}` + 'vh'
          document.cookie = `appHeight=${height}; expires=${date.UTCString}`
        })
      })
    }
    
  }
)



// 88888888888 8888888888 888b     d888 8888888b.  888             d8888 88888888888 8888888888 
//     888     888        8888b   d8888 888   Y88b 888            d88888     888     888        
//     888     888        88888b.d88888 888    888 888           d88P888     888     888        
//     888     8888888    888Y88888P888 888   d88P 888          d88P 888     888     8888888    
//     888     888        888 Y888P 888 8888888P"  888         d88P  888     888     888        
//     888     888        888  Y8P  888 888        888        d88P   888     888     888        
//     888     888        888   "   888 888        888       d8888888888     888     888        
//     888     8888888888 888       888 888        88888888 d88P     888     888     8888888888 


/*  HTML TEMPLATE
	<questionDecks->AUDIO FILE HERE</questionDecks>
*/ 

