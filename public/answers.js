// Have indicators of literal meaning and intended meaning and indicators to differentiate similarities
const answers = {
    numbers: [
        { ch: '零', pinyin: 'líng', en: 'zero', audio: '零.(🎵)', }, 
        { ch: '一', pinyin: 'yī', en: 'one', audio: '一.(🎵)', }, 
        { ch: '二', pinyin: 'èr', en: 'two', audio: '二.(🎵)', }, 
        { ch: '三', pinyin: 'sān', en: 'three', audio: '三.(🎵)', },
        { ch: '四', pinyin: 'sì', en: 'four', audio: '四.(🎵)', },
        { ch: '五', pinyin: 'wǔ', en: 'five', audio: '五.(🎵)', },
        { ch: '六', pinyin: 'liù', en: 'six', audio: '六.(🎵)', },
        { ch: '七', pinyin: 'qī', en: 'seven', audio: '七.(🎵)', },
        { ch: '八', pinyin: 'bā', en: 'eight', audio: '八.(🎵)', },
        { ch: '九', pinyin: 'jiǔ', en: 'nine', audio: '九.(🎵)', },
        { ch: '十', pinyin: 'shí', en: 'ten', audio: '十.(🎵)', },
        { ch: '百', pinyin: 'bǎi', en: 'hundred', audio: '百.(🎵)', },
        { ch: '千', pinyin: 'qiān', en: 'thousand', audio: '千.(🎵)', },
        { ch: '万', pinyin: 'wàn', en: 'ten thousand', audio: '万.(🎵)', },
        { ch: '亿', pinyin: 'yì', en: 'hundred million', audio: '亿.(🎵)', },
    ],
    pronouns: [
        { ch: '我', pinyin: 'wǒ', en: 'me, I', audio: '我.(🎵)', },
        { ch: '你', pinyin: 'nǐ', en: 'you', audio: '你.(🎵)', }, 
        { ch: '您', pinyin: 'nín', en: 'you (polite)', audio: '您.(🎵)', }, 
        { ch: '他', pinyin: 'tā (male)', en: 'he/him', audio: '他，她.(🎵)', }, 
        { ch: '她', pinyin: 'tā (female)', en: 'she', audio: '他，她.(🎵)', }, 
        { ch: '我们', pinyin: 'wǒ men', en: 'us', audio: '我们.(🎵)', }, 
        { ch: '你们', pinyin: 'nǐ men', en: 'you (plural)', audio: '你们.(🎵)', }, 
        { ch: '您们', pinyin: 'nín men', en: 'you (polite, plural)', audio: '您们.(🎵)', }, 
        { ch: '他们', pinyin: 'tā men (male)', en: 'them (male)', audio: '他们，她们.(🎵)', }, 
        { ch: '她们', pinyin: 'tā men (female)', en: 'them (female)', audio: '他们，她们.(🎵)', }, 
        { ch: '我的', pinyin: 'wǒ de', en: 'mine', audio: '我的.(🎵)', }, 
        { ch: '你的', pinyin: 'nǐ de', en: 'yours', audio: '你的.(🎵)', }, 
        { ch: '您的', pinyin: 'nín de', en: 'yours (polite)', audio: '您的.(🎵)', }, 
        { ch: '他的', pinyin: 'tā de (male)', en: 'his', audio: '他的，她的.(🎵)', }, 
        { ch: '她的', pinyin: 'tā de (female)', en: 'her/hers', audio: '他的，她的.(🎵)', }, 
        { ch: '我们的', pinyin: 'wǒ men de', en: 'ours', audio: '我们的.(🎵)', }, 
        { ch: '你们的', pinyin: 'nǐ men de', en: 'yours (plural) ', audio: '你们的.(🎵)', }, 
        { ch: '您们的', pinyin: 'nín men de', en: 'yours (polite plural)', audio: '您们的.(🎵)', }, 
        { ch: '他们的', pinyin: 'tā men de (male or both)', en: 'theirs (male or both)', audio: '他们的，她们的.(🎵)', }, /* 1 */ 
        { ch: '她们的',  pinyin: 'tā men de (female)', en: 'theirs (female)', audio: '他们的，她们的.(🎵)', }, 
    ]
    ,
    family: [
        { ch: '家庭', pinyin: 'jiā tíng', en: 'family / household', audio: '家庭.(🎵)', }, 
        { ch: '父母', pinyin: 'fù mǔ', en: 'father and mother / parents', audio: '父母.(🎵)', }, 
        { ch: '爸爸', pinyin: 'bà ba', en: 'father', audio: '爸爸.(🎵)', }, 
        { ch: '父亲', pinyin: 'fù qīn', en: 'father (honorific)', audio: '父亲.(🎵)', }, /* 1 */
        { ch: '妈妈', pinyin: 'mā mā', en: 'mother', audio: '妈妈.(🎵)', }, 
        { ch: '母亲', pinyin: 'mǔ qīn', en: 'mother (honorific)', audio: '母亲.(🎵)', },  /* 1 */
        { ch: '继母', pinyin: 'jì mǔ', en: 'step mother', audio: '继母.(🎵)', }, 
        { ch: '哥哥', pinyin: 'gē ge', en: 'older brother', audio: '哥哥.(🎵)', }, 
        { ch: '弟弟', pinyin: 'dì di', en: 'younger brother', audio: '弟弟.(🎵)', }, 
        { ch: '姐姐', pinyin: 'jiě jie', en: 'older sister', audio: '姐姐.(🎵)', }, 
        { ch: '妹妹', pinyin: 'mèi mèi', en: 'younger sister', audio: '妹妹.(🎵)', },
    
        { ch: '配偶', pinyin: 'pèi ǒu', en: 'spouse', audio: '配偶.(🎵)', }, 
        { ch: '先生', pinyin: 'xiān sheng', en: 'mister', audio: '先生.(🎵)', }, 
        { ch: '丈夫', pinyin: 'zhàng fu', en: 'husband (formal)', audio: '丈夫.(🎵)', }, 
        { ch: '老公', pinyin: 'lǎo gong', en: '(informal) husband', audio: '老公.(🎵)', }, 
        { ch: '太太', pinyin: 'tài tai', en: 'missus', audio: '太太.(🎵)', }, 
        { ch: '夫人', pinyin: 'fū rén', en: 'missus (old chinese)', audio: '夫人.(🎵)', }, 
        { ch: '妻子', pinyin: 'qī zi', en: 'wife (formal)', audio: '妻子.(🎵)', },
        { ch: '老婆', pinyin: 'lǎo pó', en: '(informal) wife', audio: '老婆.(🎵)', },
    
        { ch: '孩子', pinyin: 'hái zi', en: 'child', audio: '孩子.(🎵)', }, 
        { ch: '儿子', pinyin: 'ér zi', en: 'son', audio: '儿子.(🎵)', }, 
        { ch: '儿媳', pinyin: 'ér xí', en: 'son\'s wife', audio: '儿媳.(🎵)', },
        { ch: '女婿', pinyin: 'nǚ xù', en: 'daughter\'s husband', audio: '女婿.(🎵)', },
        { ch: '女儿', pinyin: 'nǚ ér', en: 'daughter', audio: '女儿.(🎵)', }, 
        { ch: '叔叔', pinyin: 'shū shu', en: 'uncle (non-blood related)', audio: '叔叔.(🎵)', },
        { ch: '爷爷', pinyin: 'yé ye', en: '(informal) father\'s father', audio: '爷爷.(🎵)', }, 
        { ch: '奶奶', pinyin: 'nǎi nai', en: '(informal) father\'s mother', audio: '奶奶.(🎵)', },
        { ch: '公公', pinyin: 'gōng gōng', en: '(informal) mother\'s father', audio: '公公.(🎵)', },
        { ch: '姥姥', pinyin: 'lǎo lǎo', en: '(informal) mother\'s mother', audio: '姥姥.(🎵)', }, 
        { ch: '祖父', pinyin: 'zǔ fù', en: 'father\'s father', audio: '祖父.(🎵)', }, 
        { ch: '祖母', pinyin: 'zǔ mǔ', en: 'father\'s mother', audio: '祖母.(🎵)', },
        { ch: '外祖父', pinyin: 'wài zǔ fù', en: 'mother\'s father', audio: '外祖父.(🎵)', }, 
        { ch: '外祖母', pinyin: 'wài zǔ mǔ', en: 'mother\'s mother', audio: '外祖母.(🎵)', },
        { ch: '岳父', pinyin: 'yuè fù', en: 'father-in-law', audio: '岳父.(🎵)', }, // Check for distinction between wife and husband
        { ch: '岳母', pinyin: 'yuè mǔ', en: 'mother-in-law', audio: '岳母.(🎵)', }, // Check for distinction between wife and husband
        { ch: '孙女', pinyin: 'sūn nǚ', en: 'granddaughter', audio: '孙女.(🎵)', }, // Check for distinction between father's and mother's side
        { ch: '孙子', pinyin: 'sūn zi', en: 'grandson', audio: '孙子.(🎵)', }, // Check for distincition between father's and mother's side
        
        // { ch: '外婆', pinyin: 'wài pó', en: '(informal) mother\'s mother', audio: '', }, 
        // { ch: '外公', pinyin: 'wài gōng', en: '(informal) mother\'s father', audio: '', }, 
        // { ch: '亲戚', pinyin: 'qīn qi', en: '(a) relative (i.e. family member)', audio: '', }, 
        // { ch: '姑妈', pinyin: 'gū mā', en: 'aunt', audio: '', },
        // { ch: '表兄弟', pinyin: 'biǎo xiōng dì', en: 'father\\'s sister\\'s sons / maternal male cousin', audio: '', }, 
        // { ch: '世代', pinyin: 'shì dài', en: 'generation; generation to generation', audio: '', }, 
        // { ch: '双胞胎', pinyin: 'shuāng bāo tāi', en: 'twin', audio: '', }, 
        // { ch: '堂兄弟', pinyin: 'táng xiōng dì', en: 'father\\'s brother\\'s sons; paternal male cousin', audio: '', }, 
        // { ch: '外甥', pinyin: 'wài shēng', en: 'sister\\'s son / wife\\'s sibling\\'s son', audio: '', }, 
        // { ch: '外甥女', pinyin: 'wài sheng nǚ', en: 'sister\\'s daughter / wife\\'s sibling\\'s daughter', audio: '', }, 
        // { ch: '兄弟', pinyin: 'xiōng dì', en: 'brothers / younger brother / brethren', audio: '', }, 
    ],
    time: [
        {ch: '时钟', pinyin: 'shí zhōng', en: 'clock', audio: '时钟.(🎵)', }, /* 1 */
        {ch: '闹钟', pinyin: 'nào zhōng', en: 'alarm clock', audio: '闹钟.(🎵)', }, /* 1 */
        {ch: '电子报', pinyin: 'diàn zǐ bào', en: 'digital clock', audio: '电子报.(🎵)', }, /* 1 */
        {ch: '古代', pinyin: 'gǔ dài', en: 'ancient history', audio: '古代.(🎵)', }, /* 1 */
        {ch: '沙钟，沙漏', pinyin: 'shā zhōng, shā lòu', en: 'hourglass', audio: '沙钟，沙漏.(🎵)', }, /* 1 */
        {ch: '怀表', pinyin: 'huái biǎo', en: 'pocket watch', audio: '怀表.(🎵)', }, /* 1 */
        {ch: '日晷', pinyin: 'rì guǐ', en: 'sun dial', audio: '日晷.(🎵)', }, /* 1 */
        {ch: '记事日历', pinyin: 'jì shì rì lì', en: 'schedule', audio: '记事日历.(🎵)', },
        {ch: '休息', pinyin: 'xiū xí', en: 'break', audio: '休息.(🎵)', },
        {ch: '日历', pinyin: 'rì lì', en: 'calendar', audio: '日历.(🎵)', },
        {ch: '世纪', pinyin: 'shì jì', en: 'century', audio: '世纪.(🎵)', },
        // {ch: '日期', pinyin: '', en: 'date', audio: '', },
        // {ch: '结束', pinyin: '', en: 'end', audio: '', },
        // {ch: '中世纪', pinyin: '', en: 'middle ages', audio: '', },
        // {ch: '早晨', pinyin: '', en: 'early morning', audio: '', },
        // {ch: '正点', pinyin: '', en: 'punctiality', audio: '', },
        // {ch: '时间', pinyin: '', en: 'time', audio: '', },
        
        {ch: '日出', pinyin: 'rì chū', en: 'sunrise', audio: '日出.(🎵)', },
        {ch: '夕阳', pinyin: 'xī yáng', en: 'sunset', audio: '夕阳.(🎵)', },
    
        {ch: '历史', pinyin: 'lì shǐ', en: 'history', audio: '历史.(🎵)', },
        {ch: '未来', pinyin: 'wèi lái', en: 'future', audio: '未来.(🎵)', },
        {ch: '现在', pinyin: 'xiàn zài', en: 'now', audio: '现在.(🎵)', }, // Ask what is the difference between 目前 and 现在
        {ch: '过去', pinyin: 'guò qù', en: 'past', audio: '过去.(🎵)', },
    
        // {ch: '早上', pinyin: 'zǎoshang', en: 'morning', audio: '', },
        // {ch: '下午', pinyin: 'xiàwǔ', en: 'afternoon', audio: '', },
        // {ch: '晚上', pinyin: 'wǎnshàng', en: 'evening/night', audio: '', },
        // {ch: '黄昏', pinyin: '', en: 'dusk', audio: '', },
        // {ch: '黎明', pinyin: '', en: 'dawn', audio: '', },
        
        {ch: '前天', pinyin: 'qián tiān', en: 'day before yesterday', audio: '前天.(🎵)', },
        {ch: '昨天', pinyin: 'zuó tiān', en: 'yesterday', audio: '昨天.(🎵)', },
        {ch: '今天', pinyin: 'jīn tiān', en: 'today', audio: '今天.(🎵)', },
        {ch: '明天', pinyin: 'míng tiān', en: 'tomorrow', audio: '明天.(🎵)', },
        {ch: '后天', pinyin: 'hòu tiān', en: 'day after tomorrow', audio: '后天.(🎵)', },
    
        {ch: '季节', pinyin: 'jì jié', en: 'seasons', audio: '季节.(🎵)', },
        {ch: '春天', pinyin: 'chūn tiān', en: 'spring', audio: '春天.(🎵)', },
        {ch: '夏天', pinyin: 'xià tiān', en: 'summer', audio: '夏天.(🎵)', },
        {ch: '秋天', pinyin: 'qiū tiān', en: 'autumn/fall', audio: '秋天.(🎵)', },
        {ch: '冬天', pinyin: 'dōng tiān', en: 'winter', audio: '冬天.(🎵)', },
        
        {ch: '年', pinyin: 'nián', en: 'year', audio: '年.(🎵)', },
        {ch: '月', pinyin: 'yuè', en: 'month', audio: '月.(🎵)', },
        {ch: '天', pinyin: 'tiān', en: 'day', audio: '天.(🎵)', },
        {ch: '小时', pinyin: 'xiǎo shí', en: 'hour', audio: '小时.(🎵)', },
        {ch: '分钟', pinyin: 'fēn zhōng', en: 'minute', audio: '分钟.(🎵)', },
        {ch: '秒', pinyin: 'miǎo', en: 'second', audio: '秒.(🎵)', },
        {ch: '毫秒', pinyin: 'háo miǎo', en: 'millisecond', audio: '毫秒.(🎵)', },
        {ch: '点钟', pinyin: 'diǎn zhōng', en: 'oclock', audio: '点钟.(🎵)', },
    
        {ch: '周末', pinyin: 'zhōu mò', en: 'weekend', audio: '周末.(🎵)', },
        {ch: '星期一', pinyin: 'xīng qí yī', en: 'Monday', audio: '星期一.(🎵)', },
        {ch: '星期二', pinyin: 'xīng qí èr', en: 'Tuesday', audio: '星期二.(🎵)', },
        {ch: '星期三', pinyin: 'xīng qí sān', en: 'Wednesday', audio: '星期三.(🎵)', },
        {ch: '星期四', pinyin: 'xīng qí sì', en: 'Thursday', audio: '星期四.(🎵)', },
        {ch: '星期五', pinyin: 'xīng qí wǔ', en: 'Friday', audio: '星期五.(🎵)', },
        {ch: '星期六', pinyin: 'xīng qí liù', en: 'Saturday', audio: '星期六.(🎵)', },
        {ch: '星期天', pinyin: 'xīng qí tiān', en: 'Sunday', audio: '星期天.(🎵)', },
        
        {ch: '一月', pinyin: 'yī yuè', en: 'January', audio: '一月.(🎵)', },
        {ch: '二月', pinyin: 'èr yuè', en: 'Feburary', audio: '二月.(🎵)', },
        {ch: '三月', pinyin: 'sān yuè', en: 'March', audio: '三月.(🎵)', },
        {ch: '四月', pinyin: 'sì yuè', en: 'April', audio: '四月.(🎵)', },
        {ch: '五月', pinyin: 'wǔ yuè', en: 'May', audio: '五月.(🎵)', },
        {ch: '六月', pinyin: 'liù yuè', en: 'June', audio: '六月.(🎵)', },
        {ch: '七月', pinyin: 'qī yuè', en: 'July', audio: '七月.(🎵)', },
        {ch: '八月', pinyin: 'bā yuè', en: 'August', audio: '八月.(🎵)', },
        {ch: '九月', pinyin: 'jiǔ yuè', en: 'September', audio: '九月.(🎵)', },
        {ch: '十月', pinyin: 'shí yuè', en: 'October', audio: '十月.(🎵)', },
        {ch: '十一月', pinyin: 'shí yī yuè', en: 'November', audio: '十一月.(🎵)', },
        {ch: '十二月', pinyin: 'shí èr yuè', en: 'December', audio: '十二月.(🎵)', },
    ],
    color: [
        {ch: '颜色', pinyin: 'yán sè', en: 'color', audio: '颜色.(🎵)', },
        {ch: '白色', pinyin: 'bái sè', en: 'white', audio: '白色.(🎵)', },
        {ch: '黑色', pinyin: 'hēi sè', en: 'black', audio: '黑色.(🎵)', },
        {ch: '粉红色', pinyin: 'fěn hóng sè', en: 'pink', audio: '粉红色.(🎵)', },
        {ch: '红色', pinyin: 'hóng sè', en: 'red', audio: '红色.(🎵)', },
        {ch: '蓝色', pinyin: 'lán sè', en: 'blue', audio: '蓝色.(🎵)', },
        {ch: '绿色', pinyin: 'lǜ sè', en: 'green', audio: '绿色.(🎵)', },
        {ch: '黄色', pinyin: 'huáng sè', en: 'yellow', audio: '黄色.(🎵)', },
        {ch: '紫色', pinyin: 'zǐ sè', en: 'purple', audio: '紫色.(🎵)', },
        {ch: '橙色', pinyin: 'chéng sè', en: 'orange', audio: '橙色.(🎵)', },
        {ch: '棕色', pinyin: 'zōng sè', en: 'brown', audio: '棕色.(🎵)', },
        {ch: '灰色', pinyin: 'huī sè', en: 'gray', audio: '灰色.(🎵)', },
        // {ch: '', pinyin: '', en: 'gold', audio: '', },
        // {ch: '', pinyin: '', en: 'silver', audio: '', },
        // {ch: '彩色', pinyin: 'cǎi sè', en: 'multicolor', audio: '', },
        // {ch: '五颜六色', pinyin: 'wǔ yán liù sè ', en: 'colorful', audio: '', },
        // {ch: '莹', pinyin: 'yíng', en: 'beautiful transparent color??', audio: '', }, Confirm the meaning of this word
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
    
    ],
    greetingsAndFarewells: [
        {ch: '你好', pinyin: 'nǐ hǎo', en: 'hello', audio: '你好.(🎵)', },
        {ch: '您好', pinyin: 'nín hǎo', en: 'hello (polite)', audio: '您好.(🎵)', },
        {ch: '你好吗', pinyin: 'nǐ hǎo ma？', en: 'how are you?', audio: '你好吗.(🎵)', },
        {ch: '您好吗', pinyin: 'nín hǎo ma？', en: 'how are you? (polite)', audio: '您好吗.(🎵)', },
        {ch: '拜拜', pinyin: 'bài bài', en: 'bye bye', audio: '拜拜.(🎵)', },
        {ch: '再会', pinyin: 'zài huì', en: '[we will] hello again', audio: '再会.(🎵)', },
        {ch: '明天见', pinyin: 'míng tiān jiàn', en: 'see you tomorrow', audio: '明天见.(🎵)', },
        {ch: '喂', pinyin: 'wèi', en: 'hey/hi (answering the phone)', audio: '喂.(🎵)', },
        {ch: '再见', pinyin: 'zài jiàn', en: 'goodbye', audio: '再见.(🎵)', },
        {ch: '早上好，你早，早', pinyin: 'zǎo shang hǎo, nǐ zǎo, zǎo', en: 'good morning', audio: '早上好，你早，早.(🎵)', },
        {ch: '下午好', pinyin: 'xià wǔ hǎo', en: 'good afternoon', audio: '下午好.(🎵)', },
        {ch: '晚上好', pinyin: 'wǎn shàng hǎo', en: 'good evening', audio: '晚上好.(🎵)', },
        {ch: '晚安', pinyin: 'wǎn\'ān', en: 'good night', audio: '晚安.(🎵)', },
        {ch: '你怎么样', pinyin: 'nǐ zěn me yàng?', en: 'how is it going', audio: '你怎么样.(🎵)', },
        {ch: '你的周末怎么样', pinyin: 'nǐ de zhōumò zěnme yàng', en: 'how was your weekend', audio: '你的周末怎么样.(🎵)', },
        {ch: '回头见', pinyin: 'huí tóu jiàn', en: 'see you soon', audio: '回头见.(🎵)', },
        {ch: '下个星期见', pinyin: 'xià gè xīng qí jiàn', en: 'see you next week', audio: '下个星期见.(🎵)', },
        {ch: '改天再聊', pinyin: 'gǎi tiān zài liáo', en: 'talk to you later', audio: '改天再聊.(🎵)', },
        {ch: '慢走', pinyin: 'màn zǒu', en: 'take care', audio: '慢走.(🎵)', },
        
        
    ],
    people: [
        // {ch: '学者', pinyin: 'Xuézhě', en: 'scholar', audio: '', },
        // {ch: '学生', pinyin: 'Xuéshēng', en: 'student', audio: '', },
        // {ch: '领导', pinyin: 'Lǐngdǎo', en: 'leader', audio: '', },
        // {ch: '男', pinyin: 'Nán', en: 'male', audio: '', },
        // {ch: '女', pinyin: 'Nǚ', en: 'female', audio: '', },
        // {ch: '男孩', pinyin: 'Nánhái', en: 'boy', audio: '', },
        // {ch: '女孩', pinyin: 'Nǚhái', en: 'girl', audio: '', },
        // {ch: '朋友', pinyin: 'Péngyǒu', en: 'friend', audio: '', },
        // {ch: '人', pinyin: 'Rén', en: 'people/person', audio: '', },
        // {ch: '会员', pinyin: 'Huìyuán', en: 'member', audio: '', },
        // {ch: '房东', pinyin: 'Fángdōng', en: 'landlord', audio: '', },
        // {ch: '队友', pinyin: 'Duìyǒu', en: 'teammate/parter', audio: '', },
        // {ch: '服务员', pinyin: 'Fúwùyuán', en: 'Service Worker', audio: '', },
        // {ch: '参加者', pinyin: 'Cānjiā zhě', en: 'Attendee', audio: '', },
        // {ch: '导师', pinyin: 'Dǎoshī', en: 'tutor', audio: '', },
        // {ch: '对手', pinyin: 'Duìshǒu', en: 'opponent', audio: '', },
        // {ch: '师傅', pinyin: 'Shīfù', en: 'master', audio: '', },
        // {ch: '士兵', pinyin: 'Shìbīng', en: 'soilder', audio: '', },
        // {ch: '英雄', pinyin: 'Yīngxióng', en: 'hero', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },

    ],
    
    
    jobs: [
        {ch: '技术员', pinyin: 'jì shù yuán', en: 'technician', audio: '技术员.(🎵)', },
        {ch: '出纳员', pinyin: 'chū nà yuán', en: 'cashier', audio: '出纳员.(🎵)', },
        {ch: '图书管理员', pinyin: 'tú shū guǎn lǐ yuán', en: 'librarian', audio: '图书管理员.(🎵)', },
        {ch: '接待员', pinyin: 'jiē dài yuán', en: 'receptionist', audio: '接待员.(🎵)', },
        {ch: '秘书', pinyin: 'mì shu', en: 'secretary', audio: '秘书.(🎵)', },
        {ch: '电话推销员', pinyin: 'diàn huà tuī xiāo yuán', en: 'telemarketer', audio: '电话推销员.(🎵)', },
        {ch: '推销员', pinyin: 'tuī xiāo yuán', en: 'saleman', audio: '推销员.(🎵)', },
        {ch: '分析员', pinyin: 'fēn xī yuán', en: 'analyst', audio: '分析员.(🎵)', },
        {ch: '营销人员', pinyin: 'yíng xiāo rén yuán', en: 'marketer', audio: '营销人员.(🎵)', },
        {ch: '工程师', pinyin: 'gōng chéng shī', en: 'engineer', audio: '工程师.(🎵)', },
        {ch: '老师', pinyin: 'lǎo shī', en: 'teacher', audio: '老师.(🎵)', },
        {ch: '培训师', pinyin: 'péi xùn shī', en: 'trainer', audio: '培训师.(🎵)', },
        {ch: '律师', pinyin: 'lǜ shī', en: 'lawyer', audio: '律师.(🎵)', },
        {ch: '治疗师', pinyin: 'zhì liáo shī', en: 'therapist', audio: '治疗师.(🎵)', },
        {ch: '理发师', pinyin: 'lǐ fǎ shī', en: 'barber', audio: '理发师.(🎵)', },
        {ch: '劳动者', pinyin: 'láo dòng zhě', en: 'laborer', audio: '劳动者.(🎵)', },
        {ch: '译者', pinyin: 'yì zhě', en: 'translator', audio: '译者.(🎵)', },
        {ch: '历史学家', pinyin: 'lìshǐ xué jiā', en: 'historian', audio: '历史学家.(🎵)', },
        {ch: '地质学家', pinyin: 'dì zhì xué jia', en: 'geologist', audio: '地质学家.(🎵)', },
        {ch: '艺术家', pinyin: 'yì shù jiā', en: 'artist', audio: '艺术家.(🎵)', },
        {ch: '管家', pinyin: 'guǎn jiā', en: 'housekeeper', audio: '管家.(🎵)', },
        {ch: '经纪人', pinyin: 'jīng jì rén', en: 'broker', audio: '经纪人.(🎵)', },
        {ch: '主人', pinyin: 'zhǔ rén', en: 'host (noun)', audio: '主人.(🎵)', },
        {ch: '撰稿人', pinyin: 'zhuàn gǎo rén', en: 'copywriter', audio: '撰稿人.(🎵)', },
        {ch: '机械', pinyin: 'jī xiè', en: 'mechanic', audio: '机械.(🎵)', },
        {ch: '经理', pinyin: 'jīng lǐ', en: 'manager', audio: '经理.(🎵)', },
        {ch: '人力资源', pinyin: 'rén lì zī yuán', en: 'human resourses', audio: '人力资源.(🎵)', },
        {ch: '护士', pinyin: 'hù shì', en: 'nurse', audio: '护士.(🎵)', },
        {ch: '医生', pinyin: 'yī shēng', en: 'doctor', audio: '医生.(🎵)', },
        {ch: '开发商', pinyin: 'kāi fā shāng', en: 'developer', audio: '开发商.(🎵)', },
        {ch: '教练', pinyin: 'jiào liàn', en: 'coach', audio: '教练.(🎵)', },
        {ch: '算子', pinyin: 'suàn zi', en: 'operator', audio: '算子.(🎵)', },
        {ch: '助理', pinyin: 'zhù lǐ', en: 'assistant', audio: '助理.(🎵)', },
        {ch: '警官', pinyin: 'jǐng guān', en: 'police officer', audio: '警官.(🎵)', },
        {ch: '信使', pinyin: 'xìn shǐ', en: 'mailman', audio: '信使.(🎵)', },
        {ch: '园丁', pinyin: 'yuán dīng', en: 'landscaper', audio: '园丁.(🎵)', },
        {ch: '兽医', pinyin: 'shòu yī', en: 'veterinarian', audio: '兽医.(🎵)', },
        {ch: '代客', pinyin: 'dài kè', en: 'valet', audio: '代客.(🎵)', },
        {ch: '编剧', pinyin: 'biān jù', en: 'screenwriter', audio: '编剧.(🎵)', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },
        // {ch: '', pinyin: '', en: '', audio: '', },

    ],
    vegetables: [
      // {ch: '土豆，马铃薯', pinyin: 'tǔ dòu, mǎ líng shǔ', en: 'potato', audio: '', },
	    // {ch: '胡萝卜', pinyin: 'hú luó bo', en: 'carrot', audio: '', },
	    // {ch: '玉米', pinyin: 'yù mǐ', en: 'corn', audio: '', },
	    // {ch: '大蒜', pinyin: 'dà suàn', en: 'garlic', audio: '', },
	    // {ch: '洋葱', pinyin: 'yáng cōng', en: 'onion', audio: '', },
	    // {ch: '西兰花', pinyin: 'xī lán huā', en: 'broccoli', audio: '', },
	    // {ch: '黄瓜', pinyin: 'huáng guā', en: 'cucumber', audio: '', },
	    // {ch: '夏南瓜', pinyin: 'xià nán guā', en: 'zucchini', audio: '', },
	    // {ch: '牛油果', pinyin: 'niú yóu guǒ.', en: 'avocado', audio: '', },
	    // {ch: '菠菜', pinyin: 'bō cài', en: 'spinach', audio: '', },
	    // {ch: '生菜', pinyin: 'shēng cài', en: 'lettuce', audio: '', },
	    // {ch: '灯笼椒', pinyin: 'dēng lóng jiāo', en: 'bell pepper', audio: '', },
	    // {ch: '豌豆', pinyin: 'wān dòu', en: 'pea', audio: '', },
			// {ch: '菜花', pinyin: 'cài huā', en: 'cauliflower', audio: '', },
			// {ch: '豆子', pinyin: 'dòu zi', en: 'beans', audio: '', },
			// {ch: '橄榄', pinyin: 'gǎn lǎn', en: 'olive', audio: '', },
			// {ch: '球芽甘蓝', pinyin: 'qiú yá gān lán', en: 'brussel sprouts', audio: '', },
			// {ch: '茄子', pinyin: 'qié zi', en: 'eggplant', audio: '', },
			// {ch: '甜菜', pinyin: 'tián cài', en: 'beet, beetroot', audio: '', },
			// {ch: '羽衣甘蓝', pinyin: 'yǔ yī gān lán', en: 'kale', audio: '', },
			// {ch: '朝鲜蓟', pinyin: 'cháo xiǎn jì', en: 'artichoke', audio: '', },
			// {ch: '种子', pinyin: 'zhǒng zǐ', en: 'seed', audio: '', },
			// {ch: '芋头', pinyin: 'yù tou', en: 'taro', audio: '', },
			// {ch: '白饭', pinyin: 'bái fàn', en: 'rice', audio: '', },
	    // {ch: '番茄', pinyin: 'fān qié', en: 'tomato', audio: '', },
	    // {ch: '海藻', pinyin: 'hǎi zǎo', en: 'seaweed', audio: '', },
	    // {ch: '蘑菇', pinyin: 'mó gū', en: 'mushroom', audio: '', },
	    // {ch: '芜菁', pinyin: 'wú jīng', en: 'turnip', audio: '', },
	    // {ch: '芦笋', pinyin: 'lú sǔn', en: 'asparagus', audio: '', },
	    // {ch: '卷心菜', pinyin: 'juǎn xīncài', en: 'cabbage', audio: '', },
	    // {ch: '萝卜', pinyin: 'luó bo', en: 'radish', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
	    // {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },
			// {ch: '', pinyin: '', en: '', audio: '', },

    ],
    school: [],
    animals: [],
    weather: [],
    sports: [],
    tools: [],
    vehicles: [],
    mathimatics: [
//      {ch: '加', pinyin: 'jiā', en: 'plus', audio: '', },
//      {ch: '添加', pinyin: 'tiān jiā', en: 'add, addition', audio: '', },
//      {ch: '减', pinyin: 'jiǎn', en: 'minus', audio: '', },
//      {ch: '减法', pinyin: 'jiǎn fǎ', en: 'subtraction', audio: '', },
//      {ch: '减去', pinyin: 'jiǎn qù', en: 'subtract', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//      {ch: '', pinyin: '', en: '', audio: '', },
//
    ],
    furniture: [
			{ch: '家具', pinyin: 'jiā jù', en: 'furniture', audio: '家具.(🎵)', },
			{ch: '椅子', pinyin: 'yǐ zi', en: 'chair', audio: '椅子.(🎵)', },
			{ch: '长椅', pinyin: 'cháng yǐ', en: 'bench', audio: '长椅.(🎵)', },
			{ch: '桌子', pinyin: 'zhuō zi', en: 'table', audio: '桌子.(🎵)', },
			{ch: '沙发', pinyin: 'shā fā', en: 'sofa', audio: '沙发.(🎵)', },
			{ch: '床', pinyin: 'chuáng', en: 'bed', audio: '床.(🎵)', },
			{ch: '书桌', pinyin: 'shū zhuō', en: 'desk', audio: '书桌.(🎵)', },
			{ch: '床垫', pinyin: 'chuáng diàn', en: 'mattress', audio: '床垫.(🎵)', },
			{ch: '梳妆台', pinyin: 'shū zhuāng tái', en: 'dresser', audio: '梳妆台.(🎵)', },
			{ch: '电视架', pinyin: 'diàn shì jià', en: 'TV stand', audio: '电视架.(🎵)', },
			{ch: '书柜', pinyin: 'shū guì', en: 'bookcase', audio: '书柜.(🎵)', },
			{ch: '双层床', pinyin: 'shuāng céng chuáng', en: 'bunk bed', audio: '双层床.(🎵)', },
			{ch: '凳子', pinyin: 'dèng zǐ', en: 'stool', audio: '凳子.(🎵)', },
			{ch: '榻', pinyin: 'tà', en: 'couch', audio: '榻.(🎵)', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
//			{ch: '', pinyin: '', en: '', audio: '', },
    ],
    building: [],
    electric: [],
    plumbing: [],
    clothes: [],
    materials: [],
    electronics: [],
    verbs: [],
    adjectives: [],
    phrases: [],
    office: [],
    government: [],
    geography: [],
    medical: [],
}

// May require images to bridge the indirect translations of vegetables
// INDICATE MALE AND FEMALE AND FORMAL AND POLITE
// /* 1 */ Thanks to Selene from Hellotalk app for helping out with some of the translations
